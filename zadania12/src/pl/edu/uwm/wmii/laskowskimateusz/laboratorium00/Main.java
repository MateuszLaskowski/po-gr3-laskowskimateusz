package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main
{
    public static void main(String[] args)
    {
        System.out.println();
        Tasks tasks = new Tasks();
        Task task0 = new Task(0, "terst");
        Task task1 = new Task(3, "test1");
        Task task2 = new Task(8, "test2");
        Task task3 = new Task(3, "test3");
        Task task4 = new Task(0, "test test");
        tasks.dodaj(task0);
        tasks.dodaj(task1);
        tasks.dodaj(task2);
        tasks.dodaj(task3);
        tasks.dodaj(task4);

        tasks.nastepne();
        System.out.println("\n");
        StudentDB db = new StudentDB();
        Student student1 = new Student("Carl", "Laskowski2");
        db.add(new Student("Susan", "Laskowski1"), "db");
        db.add(student1, "db+");
        db.add(new Student("Joe", "Laskowski3"), "bdb");
        System.out.println(db);

        System.out.println();
        Map<Integer, HashSet<String>> map = new HashMap<>();
        try
        {
            File file = new File("file.txt");
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext())
            {
                ArrayList<String> words = new ArrayList<>(Arrays.asList(scanner.next().split(" ")));
                for (String word : words) {
                    int tmp = word.hashCode();
                    if (map.containsKey(tmp)) {
                        map.get(word.hashCode()).add(word);
                    } else {
                        HashSet<String> subMap = new HashSet<>();
                        map.put(word.hashCode(), subMap);
                    }
                }
            }
            for (Integer integer : map.keySet()) {
                if (map.get(integer).size() == 1) {
                    System.out.println(map.get(integer));
                }
            }
        }
        catch (FileNotFoundException ignored)
        {
        }
    }
}