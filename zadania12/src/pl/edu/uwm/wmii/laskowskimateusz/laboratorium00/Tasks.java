package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.PriorityQueue;


public class Tasks
{
    PriorityQueue<Task> tasks;

    public Tasks()
    {
        tasks = new PriorityQueue<>();
    }

    public void dodaj(Task task)
    {
        tasks.add(task);
    }

    public void zakoncz()
    {
        System.exit(0);
    }

    public void nastepne()
    {
        Task tmp = tasks.poll();
        System.out.print("");
    }
}