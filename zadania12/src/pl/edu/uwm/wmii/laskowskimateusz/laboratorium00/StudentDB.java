package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;


import java.util.*;

public class StudentDB
{
    HashMap<Integer, Student> db1;
    TreeMap<Student, String> db2;

    public StudentDB()
    {
        db1 = new HashMap<>();
        db2 = new TreeMap<>();
    }

    public void add(Student student, String grade)
    {
        db1.put(student.id, student);
        db2.put(student, grade);
    }

    public void remove(Student student)
    {
        Student sToR = db1.get(student.id);
        db1.remove(sToR.id);
        db2.remove(sToR);
    }

    public void change(Student student, String grade)
    {
        Student sToR = db1.get(student.id);
        db2.replace(sToR, grade);
    }

    @Override
    public String toString()
    {
        StringBuilder result = new StringBuilder();
        for (Student student : db2.keySet()) {
            result.append(student.name).append(": ").append(db2.get(student)).append("\n");
        }
        return result.toString();
    }
}