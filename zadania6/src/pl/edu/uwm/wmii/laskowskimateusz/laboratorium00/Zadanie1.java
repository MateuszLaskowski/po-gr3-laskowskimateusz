package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

class RachunekBankowy{

    private static double rocznaStopaProcentowa;
    double saldo;

    void obliczMiesieczneOdsetki()
    {
        saldo += (saldo * rocznaStopaProcentowa/100)/12;
    }

    void setRocznaStopaProcentowa(double x)
    {
        rocznaStopaProcentowa = x;
    }
}

public class Zadanie1 {

    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy();
        RachunekBankowy saver2 = new RachunekBankowy();

        saver1.saldo = 2000.00;
        saver2.saldo = 3000.00;

        saver1.setRocznaStopaProcentowa(4);

        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();

        System.out.print(String.valueOf(saver1.saldo) + '\n');
        System.out.print(String.valueOf(saver2.saldo) + '\n');

        saver1.setRocznaStopaProcentowa(5);

        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();

        System.out.print(String.valueOf(saver1.saldo) + '\n');
        System.out.print(String.valueOf(saver2.saldo) + '\n');
    }
}
