package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.ArrayList;

class IntegerSet {
    public ArrayList<Integer> tmp;

    public void union(ArrayList<Integer> newList) {
        tmp.addAll(newList);
    }

    public void intersection(ArrayList<Integer> newList) {
        tmp.retainAll(newList);
    }

    void insertElement (int value) {
        for(int i=0; i<tmp.size(); i++)
        {
            if(tmp.get(i) == value) {
                tmp.remove(i);
                break;
            }
        }
    }

    public String toString() {
        return tmp.toString();
    }

    public void deleteElement(int value) {
        for(int i=0; i<tmp.size(); i++)
        {
            if(tmp.get(i) == value)
            {
                tmp.remove(value);
            }
        }
    }
    public void equals(ArrayList<Integer> newList) {
        Boolean bool = tmp.equals(newList);
    }
}

public class Zadanie2 {
    public static void main(String[] args) {
        IntegerSet test = new IntegerSet();

        ArrayList<Integer> list = new ArrayList<>();
        ArrayList<Integer> newL = new ArrayList<>();

        for(int i=0; i<100; i++)
        {
            list.add(i);
            newL.add(i + 2);
        }

        test.tmp = list;

        test.union(newL);
        test.intersection(newL);
        test.insertElement(3);
        test.deleteElement(5);
        test.toString();
        test.equals(newL);
    }
}
