package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.ArrayList;

public class Zadanie4 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        ArrayList<Integer> wynik = reversed(a);
        System.out.print(wynik);
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> new_array = new ArrayList<>();
        for (int i = a.size()-1; i>-1; i--){
            new_array.add(a.get(i));
        }
        return new_array;
    }
}