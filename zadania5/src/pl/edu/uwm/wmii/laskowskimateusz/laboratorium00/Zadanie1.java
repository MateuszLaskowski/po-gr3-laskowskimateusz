package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.ArrayList;

public class Zadanie1 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        ArrayList<Integer> b = new ArrayList<>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);

        ArrayList<Integer> wynik = append(a, b);
        System.out.print(wynik);
    }
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> new_array = new ArrayList<>();
        new_array.addAll(a);
        new_array.addAll(b);
        return new_array;
    }
}
