package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.ArrayList;

public class Zadanie2 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        ArrayList<Integer> b = new ArrayList<>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);

        ArrayList<Integer> wynik = merge(a, b);
        System.out.print(wynik);
    }
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> new_array = new ArrayList<>();
        int count_a = 0;
        int count_b = 0;
        for (int i = 0; i<a.size()+b.size();i++){
            if(i % 2 == 0 && i < a.size()*2){
                new_array.add(a.get(count_a));
                count_a++;
            }
            else if (i < b.size()*2){
                new_array.add(b.get(count_b));
                count_b++;
            }
        }
        return new_array;
    }
}
