package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.ArrayList;
import java.util.Collections;

public class Zadanie3 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        ArrayList<Integer> b = new ArrayList<>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);

        ArrayList<Integer> wynik = mergeSorted(a, b);
        System.out.print(wynik);
    }
    
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> new_array = new ArrayList<>();
        Collections.sort(a);
        Collections.sort(b);

        while (a.size() > 0 && b.size() > 0){
            new_array.add(Math.min(Collections.min(a), Collections.min(b)));
            if (Collections.min(a) >= Collections.min(b)){
                b.remove(0);
            }
            else {
                a.remove(0);
            }
        }
        if (a.size() > b.size()){
            new_array.addAll(a);
        }
        new_array.addAll(b);
        return new_array;
    }
}