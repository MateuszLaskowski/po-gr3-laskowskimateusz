package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.ArrayList;

public class Zadanie5 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        System.out.println(a);
        reverse(a);
        System.out.print(a);

    }
    public static void reverse(ArrayList<Integer> a){
        ArrayList<Integer> new_array = new ArrayList<>(a);
        a.clear();
        for (int i = new_array.size()-1; i>-1; i--){
            a.add(new_array.get(i));
        }
    }
}