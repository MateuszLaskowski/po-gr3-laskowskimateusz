package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.time.chrono.ChronoLocalDate;
import java.util.*;

public class Main
{
    static public void main(String[] args)
    {
        System.out.println("\n");
        LinkedList<Integer> pracownicy = new LinkedList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        Utils.redukuj(pracownicy, 2);
        System.out.println(pracownicy);
        System.out.println("\n");
        LinkedList<Integer> lista = new LinkedList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        Utils.odwroc(lista);
        System.out.println(lista);
        System.out.println("\n");
        {
            Stack<String> stack = new Stack<>();
            String string = "Ala ma kota. Jej kot lubi myszy.";
            StringBuilder tmp = new StringBuilder();
            for (int i = 0; i < string.length(); i++)
            {
                char character = string.charAt(i);
                if (character == ' ' && tmp.toString().equals(""))
                    continue;

                if ((character == ' ' || character == '.'))
                {
                    stack.push(tmp.toString());
                    if (character == '.')
                        stack.push(String.valueOf(character));
                    tmp = new StringBuilder();
                }
                else
                {
                    tmp.append(Character.toLowerCase(character));
                }
            }

            tmp = new StringBuilder();
            StringBuilder main = new StringBuilder();
            while (stack.size() != 0)
            {

                String substring = stack.pop();
                if (substring.equals(".") || stack.size() == 0)
                {
                    if (tmp.toString().equals(""))
                        continue;
                    if (stack.size() == 0)
                    {
                        tmp.append(" ").append(substring);
                    }
                    main.insert(0, tmp.substring(1, 2).toUpperCase() + tmp.substring(2) + ". ");
                    tmp = new StringBuilder();
                }
                else
                {
                    tmp.append(" ").append(substring);
                }
            }
            System.out.println(main);
        }
        System.out.println("\n");
        int number = 2015;
        Stack<Integer> stack = new Stack<>();
        while (number != 0)
        {
            stack.push(number % 10);
            number /= 10;
        }
        while (!stack.isEmpty())
        {
            System.out.print(stack.pop());
        }
        System.out.println("\n");
        int n = 21;
        ArrayList<Integer> primes = new ArrayList<>();
        for (int i = 2; i < n; i++)
        {
            primes.add(i);
        }
        for (int i = 2; i < n; i++)
        {
            for (int j = 0; j < primes.size(); j++)
            {
                if (primes.get(j)%i == 0 && primes.get(j) != i)
                {
                    primes.remove(j);
                    j--;
                }
            }
        }
        System.out.println(primes);
        System.out.println("\n");
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList("mateusz1", "mateusz2", "mateusz3", "mateusz4"));
        Utils.print(arrayList);
        stack.add(9);
        stack.add(9);
        stack.add(7);
        Utils.print(stack);

    }
}