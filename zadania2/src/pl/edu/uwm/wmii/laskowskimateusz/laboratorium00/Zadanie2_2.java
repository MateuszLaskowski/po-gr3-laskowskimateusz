package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Scanner;

public class Zadanie2_2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        int suma = 0;
        for(int i=0; i<n; i++){
            int myinput = scan.nextInt();
            if(myinput % 2 == 0){
                suma = suma + (myinput*2);
            }
        }
        System.out.printf( "Podwójna suma liczb parzystcyh = %d\n", suma );
    }
}
