package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Scanner;

public class Zadanie2_3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        int dodatnie = 0;
        int ujemne = 0;
        int zera = 0;
        for(int i=0; i<n; i++){
            int myinput = scan.nextInt();
            if(myinput % 2 != 0){
                ujemne++;
            }
            if(myinput % 2 == 0){
                dodatnie++;
            }
            if(myinput == 0){
                zera++;
            }
        }
        System.out.printf( "Ilość liczb dodatnich = %d\n", dodatnie );
        System.out.printf( "Ilość liczb ujemnych = %d\n", ujemne );
        System.out.printf( "Ilość zer = %d\n", zera );
    }
}
