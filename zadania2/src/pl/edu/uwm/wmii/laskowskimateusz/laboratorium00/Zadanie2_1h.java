package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Scanner;

public class Zadanie2_1h {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        int licznik = 0;
        for(int i=1; i<=n; i++){
            int myinput = scan.nextInt();
            if(Math.abs(myinput) < Math.pow(i, 2)){
                licznik++;
            }
        }
        System.out.printf( "Ilość liczb spełniajacyh warunek = %d\n", licznik );
    }
}
