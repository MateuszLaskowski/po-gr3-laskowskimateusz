package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;
import java.util.Scanner;

public class Zadanie1_1h {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        double suma = 0;
        int i = 0;
        while(i<n){
            int myinput = scan.nextInt();
            suma = suma + (myinput * Math.pow(-1, i));
            i = i+1;
        }
        System.out.printf( "Sum = %f\n", suma );
    }
}
