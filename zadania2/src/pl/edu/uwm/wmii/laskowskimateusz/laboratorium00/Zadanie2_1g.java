package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Scanner;

public class Zadanie2_1g {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        int licznik = 0;
        for(int i=0; i<n; i++){
            int myinput = scan.nextInt();
            if(myinput % 2 != 0 && myinput > 0){
                licznik++;
            }
        }
        System.out.printf( "Ilość liczb spełniających warunek= %d\n", licznik );
    }
}
