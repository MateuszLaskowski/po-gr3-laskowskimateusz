package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Scanner;

public class Zadanie2_5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        int licznik = 0;
        int[] tab = new int[n];
        for(int i=0; i<n; i++){
            int myinput = scan.nextInt();
            tab[i] = myinput;
            if(i>0 && tab[i-1] > 0 && tab[i] > 0){
                licznik++;
            }
        }
        System.out.printf( "Ilość par = %d\n", licznik );
    }
}
