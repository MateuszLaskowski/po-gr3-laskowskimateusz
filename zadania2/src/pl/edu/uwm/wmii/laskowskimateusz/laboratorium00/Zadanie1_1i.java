package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;
import java.util.Scanner;

public class Zadanie1_1i {

    public static int silnia_rekurencyjna (int n) {
        if (n==0)
            return 1;
        else
            return (n*silnia_rekurencyjna(n-1));
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        double suma = 0;
        int i = 0;
        while(i<n){
            int myinput = scan.nextInt();
            i = i+1;
            suma = suma + ((myinput * Math.pow(-1, i))/ silnia_rekurencyjna(i));
        }
        System.out.printf( "Sum = %f\n", suma );
    }
}
