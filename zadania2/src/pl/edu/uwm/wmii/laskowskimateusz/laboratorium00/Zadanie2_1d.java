package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Scanner;

public class Zadanie2_1d {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        int licznik = 0;
        int[] tablica = new int[n];
        for(int i=0; i<n; i++){
            int myinput = scan.nextInt();
            tablica[i] = myinput;
            if(i > 1 && tablica[i - 1] < (tablica[i - 2] + tablica[i]) / 2){
                licznik++;
            }
        }
        System.out.printf( "Ilość liczb spełniających warunek = %d\n", licznik );
    }
}
