package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.lang.management.MonitorInfo;
import java.util.Scanner;

public class Zadanie2_4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        int MIN = scan.nextInt();
        int MAX = MIN;
        for(int i=0; i<n-1; i++){
            int myinput = scan.nextInt();
            if(myinput < MIN){
              MIN = myinput;
            }
            if(myinput > MAX){
                MAX = myinput;
            }
        }
        System.out.printf( "Najmniejsza = %d\n", MIN );
        System.out.printf( "Największa = %d\n", MAX );
    }
}
