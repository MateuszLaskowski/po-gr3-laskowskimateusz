package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;
import java.util.Scanner;

public class Zadanie1_1g {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        int sumadodawanie = 0;
        double sumamnozenie = 1;
        while(n>0){
            int myinput = scan.nextInt();
            sumamnozenie = sumamnozenie * myinput;
            sumadodawanie = sumadodawanie + myinput;
            n = n-1;
        }
        System.out.printf( "Sum = %d\n", sumadodawanie );
        System.out.printf( "Sum = %f\n", sumamnozenie );
    }
}
