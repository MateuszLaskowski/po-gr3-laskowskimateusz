package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;
import java.util.Scanner;

public class Zadanie1_1f {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        double suma = 1;
        while(n>0){
            int myinput = scan.nextInt();
            suma = suma * Math.pow(myinput, 2);
            n = n-1;
        }
        System.out.printf( "Sum = %f\n", suma );
    }
}
