package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Scanner;

public class Zadanie2_1e {
    public static int silnia_rekurencyjna (int n) {
        if (n==0)
            return 1;
        else
            return (n*silnia_rekurencyjna(n-1));
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        int licznik = 0;
        for(int i=1; i<=n; i++){
            int myinput = scan.nextInt();
            if((Math.pow(2, i) < myinput) && (myinput < silnia_rekurencyjna(i))){
                licznik++;
            }
        }
        System.out.printf( "Ilość liczb spełniających warunek = %d\n", licznik );
    }
}
