package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;
import java.util.Scanner;

public class Zadanie1_1d {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        double suma = 0;
        while(n>0){
            int myinput = scan.nextInt();
            suma = suma + Math.sqrt(Math.abs(myinput));
            n = n-1;
        }
        System.out.printf( "Sum = %f\n", suma );
    }
}
