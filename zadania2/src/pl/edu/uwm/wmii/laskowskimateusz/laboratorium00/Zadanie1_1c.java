package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;
import java.util.Scanner;

public class Zadanie1_1c {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną n");
        int n = scan.nextInt();
        int suma = 0;
        while(n>0){
            int myinput = scan.nextInt();
            suma = suma + Math.abs(myinput);
            n = n-1;
        }
        System.out.printf( "Sum = %d\n", suma );
    }
}
