class Adres {
    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, int kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }
    public void show(){
        System.out.println(kod_pocztowy + " " + miasto);
        System.out.println("ul." + ulica + " " + numer_domu + "/" + numer_mieszkania);
    }
    public boolean przed(int other){
        return this.kod_pocztowy > other;
    }

    private final String ulica, miasto;
    private final int numer_domu;
    private final int numer_mieszkania;
    public int kod_pocztowy;
}

public class Zadanie2 {
    public static void main(String[] args){
        Adres adres1 = new Adres("Tomasza", 12, 4, "Olsztyn", 11410);
        Adres adres2 = new Adres("Tomasza", 12, 4, "Olsztyn", 10389);
        adres1.show();
        boolean x = adres1.przed(adres2.kod_pocztowy);
        System.out.println(x);
    }
}
