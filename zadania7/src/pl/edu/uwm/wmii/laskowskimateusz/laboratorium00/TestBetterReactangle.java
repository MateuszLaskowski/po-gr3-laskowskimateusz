package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import pl.imiajd.laskowski.BetterRectangle;

public class TestBetterReactangle {
    public static void main(String[] args)
    {
        BetterRectangle rectangle = new BetterRectangle(0, 0, 10, 15);
        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle.getArea());
    }
}
