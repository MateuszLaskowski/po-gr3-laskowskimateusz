package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import pl.imiajd.laskowski.NazwanyPunkt;
import pl.imiajd.laskowski.Punkt;

public class TestNazwanyPunkt {
    public static void main(String[] args) {
        NazwanyPunkt a = new NazwanyPunkt(3, 5, "port");
        a.show();

        Punkt b = new Punkt(3, 5);
        b.show();

        NazwanyPunkt c = new NazwanyPunkt(3, 6, "tawerna");
        c.show();

        a = c;
        a.show();
    }
}
