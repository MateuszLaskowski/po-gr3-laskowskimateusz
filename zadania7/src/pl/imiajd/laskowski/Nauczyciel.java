package pl.imiajd.laskowski;

public class Nauczyciel extends Osoba{
    public Nauczyciel(String nazwisko, int rokUrodzenia, int pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }
    private final int pensja;
}
