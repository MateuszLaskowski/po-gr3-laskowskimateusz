package pl.imiajd.laskowski;

public class Adres {
    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, int kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }
    public void show(){
        System.out.println(kod_pocztowy + " " + miasto);
        System.out.println("ul." + ulica + " " + numer_domu + "/" + numer_mieszkania);
    }
    public boolean przed(int other){
        return this.kod_pocztowy > other;
    }

    private final String ulica, miasto;
    private final int numer_domu, numer_mieszkania, kod_pocztowy;
}