package pl.imiajd.laskowski;

public class Osoba {
    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    private final String nazwisko;
    private final int rokUrodzenia;
}
