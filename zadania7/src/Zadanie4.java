class Osoba{
    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    @Override
    public String toString()
    {
        String result = "";
        result += "Nazwisko: " + this.nazwisko;
        result += "\nRok Urodzenia: " + this.rokUrodzenia;
        return result;
    }

    public String getNazwisko()
    {
        return nazwisko;
    }

    public int getRokUrodzenia()
    {
        return rokUrodzenia;
    }

    private final String nazwisko;
    private final int rokUrodzenia;
}

class Student extends Osoba{
    public Student(String nazwisko, int rokUrodzenia, String kierunek) {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    @Override
    public String toString()
    {
        String result = "";
        result += super.toString();
        result += "\nKierunek: " + this.kierunek;
        return result;
    }

    public String getKierunek()
    {
        return kierunek;
    }

    private final String kierunek;
}

class Nauczyciel extends Osoba {
    public Nauczyciel(String nazwisko, int rokUrodzenia, int pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    @Override
    public String toString()
    {
        String result = "";
        result += super.toString();
        result += "\nPensja: " + this.pensja;
        return result;
    }

    public int getPensja()
    {
        return pensja;
    }

    private final int pensja;
}

public class Zadanie4 {
    public static void main(String[] args){
        Student st1 = new Student("Laskowski", 2000, "Informatyka");
        Nauczyciel na1 = new Nauczyciel("Tomczak", 2000, 15000);
        System.out.println(st1);
        System.out.println(na1);
    }
}
