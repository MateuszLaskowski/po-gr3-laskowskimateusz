import pl.imiajd.laskowski.Osoba;
import pl.imiajd.laskowski.Student;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Main
{
    public static void main(String[] args)
    {
        ArrayList<Osoba> list = new ArrayList<>(Arrays.asList(
                new Osoba("nazwisko", LocalDate.of(2000, 8, 12)),
                new Osoba("nazwisko2", LocalDate.of(1999, 11, 28)),
                new Osoba("nazwisko3", LocalDate.of(1998, 5, 2)),
                new Osoba("nazwisko4", LocalDate.of(2001, 1, 14)),
                new Osoba("nazwisko5", LocalDate.of(1996, 2, 4))
        ));

        System.out.println(list);

        list.sort(Comparator.naturalOrder());

        System.out.println(list);

        System.out.println("\n\n");


        ArrayList<Student> list2 = new ArrayList<>(Arrays.asList(
                new Student("nazwisko6", LocalDate.of(2001, 3, 11), 3.0),
                new Student("nazwisko7", LocalDate.of(1999, 1, 17), 3.5),
                new Student("nazwisko9", LocalDate.of(1999, 10, 9), 3.0),
                new Student("nazwisko10", LocalDate.of(2000, 11, 22), 4.0),
                new Student("nazwisko11", LocalDate.of(1997, 7, 1), 5.0)
        ));

        System.out.println(list2);

        list2.sort(Comparator.naturalOrder());

        System.out.println(list2);

        System.out.println("\n\n");

        try
        {
            BufferedReader buffer = new BufferedReader(new FileReader(args[0]));
            ArrayList<String> result = new ArrayList<>();
            String line = buffer.readLine();
            while (line != null)
            {
                result.add(line);
                line = buffer.readLine();
            }
            buffer.close();

            System.out.println(result);
            Collections.sort(result);
            System.out.println(result);

        }
        catch (Exception ignored) {
        }
    }
}