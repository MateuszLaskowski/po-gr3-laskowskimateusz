package pl.imiajd.laskowski;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable<Osoba>
{
    private double sredniaOcen;

    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen)
    {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int compareTo(Osoba o) {
        return super.compareTo(o) + Double.compare(this.sredniaOcen, ((Student)o).sredniaOcen);
    }

    @Override
    public String toString() {
        return super.toString() + " średnia = " + sredniaOcen;
    }
}