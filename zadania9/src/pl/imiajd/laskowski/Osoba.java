package pl.imiajd.laskowski;

import java.time.LocalDate;
import java.util.Objects;

public class Osoba implements Cloneable, Comparable<Osoba>
{
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osoba(String nazwisko, LocalDate dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    @Override
    public int compareTo(Osoba o) {
        int tmp1 = this.nazwisko.compareTo(o.nazwisko);
        int tmp2 = this.dataUrodzenia.compareTo(o.dataUrodzenia);
        return  tmp1 + tmp2;
    }

    @Override
    public String toString() {
        return nazwisko + " " + dataUrodzenia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Osoba osoba = (Osoba) o;
        return Objects.equals(nazwisko, osoba.nazwisko) && Objects.equals(dataUrodzenia, osoba.dataUrodzenia);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}