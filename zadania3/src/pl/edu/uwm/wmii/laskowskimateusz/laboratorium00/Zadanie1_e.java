package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1_e {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        if (n>0 && n<101){
            int[] tablica =  new int[n];
            int max_fragment_dodatnich = 0;
            int licznik = 0;
            Random r = new Random();
            int range = 999 + 999 + 1;
            for (int i = 0; i<n; i++) {
                int int_random = r.nextInt(range)-999;
                tablica[i] = int_random;
                if(tablica[i] > 0){
                    licznik++;
                }
                else {
                    if(licznik > max_fragment_dodatnich){
                        max_fragment_dodatnich = licznik;
                    }
                    licznik = 0;
                }
                System.out.printf( "%d\n", tablica[i]);
            }
            System.out.printf("Najdłuższy fragment = %d\n", max_fragment_dodatnich);

        }
        else {
            System.out.println("Number out of range");
        }
    }
}