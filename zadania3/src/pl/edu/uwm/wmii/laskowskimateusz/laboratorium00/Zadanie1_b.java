package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1_b {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        if (n>0 && n<101){
            int[] tablica =  new int[n];
            int dodatnie = 0;
            int zera = 0;
            Random r = new Random();
            int range = 999 + 999 + 1;
            for (int i = 0; i<n; i++) {
                int int_random = r.nextInt(range)-999;
                tablica[i] = int_random;
                if(tablica[i] == 0){
                    zera++;
                }
                if(tablica[i] > 0){
                    dodatnie++;
                }
                System.out.printf( "%d\n", tablica[i] );
            }
            System.out.printf("Dodatnie = %d\n", dodatnie);
            System.out.printf("Ujemne = %d\n", n - dodatnie - zera);
            System.out.printf("Zera = %d\n", zera);
        }
        else {
            System.out.println("Number out of range");
        }
    }
}