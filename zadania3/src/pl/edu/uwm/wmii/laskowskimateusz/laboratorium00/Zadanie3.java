package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Random;

public class Zadanie3 {
    public static void main(String[] args){
        Random r = new Random();
        int range = 10 - 1 + 1;
        int m = r.nextInt(range);
        int n = r.nextInt(range);
        int k = r.nextInt(range);
        int[][] a = new int [m][n];
        generuj(a, m, n);
        wypisz(a, m, n);
        int[][] b = new int [n][k];
        generuj(b, n, k);
        wypisz(b, n, k);
        int[][] c = new int[n][k];
        mnozenie(c, b, n, k, a, m, n);
        wypisz(c, m, k);
    }
    public static void generuj(int[][] tab, int x, int y){
        Random r = new Random();
        for(int i = 0; i<x; i++){
            for(int j = 0; j<y; j++){
                tab[i][j] = r.nextInt(10);
            }
        }
    }
    public static void wypisz(int[][] tab, int x, int y){
        for(int i = 0; i<x; i++){
            for(int j = 0; j<y; j++){
                System.out.print(tab[i][j] + " ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
    }
    public static void mnozenie(int[][] tab3, int[][] tab2, int x2, int y2, int[][] tab1, int x1, int y1){
        for (int i = 0; i<x1; i++){
            for (int j = 0; j<y2; j++){
                for (int k = 0; k<x2; k++){
                    tab3[i][j] += tab1[i][k] * tab2[k][j];
                }
            }
        }
    }

}
