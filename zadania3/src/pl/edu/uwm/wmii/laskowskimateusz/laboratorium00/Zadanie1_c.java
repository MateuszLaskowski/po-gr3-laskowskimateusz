package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1_c {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        if (n>0 && n<10100){
            int[] tablica =  new int[n];
            Random r = new Random();
            int ile_MAX = 0;
            int range = 999 + 999 + 1;
            for (int i = 0; i<n; i++) {
                int int_random = r.nextInt(range)-999;
                tablica[i] = int_random;
                System.out.printf( "%d\n", tablica[i] );
            }
            int MAX = tablica[0];
            for (int i = 1; i<n-1; i++) {
                if (tablica[i] > MAX){
                    MAX = tablica[i];
                }
            }
            for (int i=0; i<n; i++){
                if (tablica[i] == MAX){
                    ile_MAX++;
                }
            }
            System.out.printf("Element maksmalny = %d\n", MAX);
            System.out.printf("Ilosc elementu makszmalny = %d\n", ile_MAX);

        }
        else {
            System.out.println("Number out of range");
        }
    }
}