package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1_g {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj n: ");
        int n = scan.nextInt();
        System.out.println("Podaj lewy: ");
        int lewy = scan.nextInt();
        System.out.println("Podaj prawy: ");
        int prawy = scan.nextInt();
        if (n < 101 && lewy >= 1 && lewy < n && prawy >= 1 && prawy < n){
            int[] tablica =  new int[n];
            Random r = new Random();
            int range = 999 + 999 + 1;
            for (int i = 0; i<n; i++) {
                int int_random = r.nextInt(range)-999;
                tablica[i] = int_random;
                System.out.printf( "%d\n", tablica[i]);
            }
            for(int i = 0;i < (prawy-lewy + 1)/2;i++){
                int temp = tablica[lewy + i];
                tablica[lewy + i] = tablica[prawy - i];
                tablica[prawy - i] = temp;
            }
            System.out.println("\n");
            for(int i = 0;i<n;i++){
                System.out.printf("%d\n", tablica[i]);
            }
        }
        else {
            System.out.println("Number out of range");
        }
    }
}