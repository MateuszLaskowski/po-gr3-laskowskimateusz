package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1_f {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        if (n>0 && n<101){
            int[] tablica =  new int[n];
            Random r = new Random();
            int range = 999 + 999 + 1;
            for (int i = 0; i<n; i++) {
                int int_random = r.nextInt(range)-999;
                tablica[i] = int_random;
                System.out.printf( "%d\n", tablica[i]);
                if(tablica[i] >0){
                    tablica[i]= 1;
                }
                else{
                    tablica[i] = -1;
                }
            }
            for(int i = 0; i<n; i++){
                System.out.printf( "%d, ", tablica[i]);
            }
        }
        else {
            System.out.println("Number out of range");
        }
    }
}