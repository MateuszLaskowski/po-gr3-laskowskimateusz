package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        if (n > 0 && n < 101) {
            int[] tablica = new int[n];
            generuj(tablica, n, -999,999);
            wypisz(tablica);
            System.out.printf("IleNieparzystych = %d\n", ileNieparzystych(tablica));
            System.out.printf("IleParzystych =  %d\n", ileParzystych(tablica));
            System.out.printf("IleDodatnich = %d\n", ileDodatnich(tablica));
            System.out.printf("IleUjemnych =  %d\n", ileUjemnych(tablica));
            System.out.printf("IleZerowych = %d\n", ileZerowych(tablica));
            System.out.printf("IleMaksymalnych = %d\n", ileMaksymalnych(tablica));
            System.out.printf("SumaDodatnich = %d\n", sumaDodatnich(tablica));
            System.out.printf("SumaUjemnych = %d\n", sumaUjemnych(tablica));
            System.out.printf("DlugoscMaksymalnegoCiaguDodatnich = %d\n", dlugoscMaksymalnegoCiaguDodatnich(tablica));
            signum(tablica);
            odwrucFragment(tablica, 1, 4);
        }
    }
    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        int range = maxWartosc - minWartosc + 1;
        for (int i = 0; i < n; i++) {
            int int_random = r.nextInt(range) + minWartosc;
            tab[i] = int_random;
        }
    }
    public static void wypisz(int[] tab){
        for (int i : tab)
        {
            System.out.printf("%d\n", i);
        }
    }
    public static int ileNieparzystych(int[] tab){
        int count = 0;
        for (int i : tab) {
            if ((i & 2) != 0) {
                count++;
            }
        }
        return count;
    }
    public static int ileParzystych(int[] tab){
        int count = 0;
        for (int i : tab) {
            if ((i & 2) == 0) {
                count++;
            }
        }
        return count;
    }
    public static int ileDodatnich(int[] tab){
        int count = 0;
        for (int i : tab) {
            if (i > 0) {
                count++;
            }
        }
        return count;
    }
    public static int ileUjemnych(int[] tab){
        int count = 0;
        for (int i : tab) {
            if (i < 0) {
                count++;
            }
        }
        return count;
    }
    public static int ileZerowych(int[] tab){
        int count = 0;
        for (int i : tab) {
            if (i == 0) {
                count++;
            }
        }
        return count;
    }
    public static int ileMaksymalnych(int[] tab){
        int MAX = tab[0];
        int ile_MAX = 0;
        for (int i : tab)
        {
            if (i == MAX)
                ile_MAX++;
            else if (i > MAX)
            {
                MAX = i;
                ile_MAX = 1;
            }
        }
        return ile_MAX;
    }
    public static int sumaDodatnich(int[] tab){
        int dodatnie = 0;
        for(int i : tab){
            if(i > 0){
                dodatnie += i;
            }
        }
        return dodatnie;
    }
    public static int sumaUjemnych(int[] tab){
        int ujemne = 0;
        for(int i : tab){
            if(i < 0){
                ujemne += i;
            }
        }
        return ujemne;
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tab){
        int licznik = 0;
        int max_fragment_dodatnich = 0;
        for (int i: tab){
            if(i > 0){
                licznik++;
            }
            else {
                if(licznik > max_fragment_dodatnich){
                    max_fragment_dodatnich = licznik;
                }
                licznik = 0;
            }
        }
        return max_fragment_dodatnich;
    }
    public static void signum(int[] tab){
        for (int i = 0;i< tab.length; i++){
            if(tab[i] >0){
                tab[i]= 1;
            }
            else{
                tab[i] = -1;
            }
        }
        wypisz(tab);
    }
    public static void odwrucFragment(int[] tab, int lewy, int prawy){
        System.out.print("\n");
        for(int i = 0;i < (prawy-lewy + 1)/2;i++){
            int temp = tab[lewy + i];
            tab[lewy + i] = tab[prawy - i];
            tab[prawy - i] = temp;
        }
        wypisz(tab);
    }
}