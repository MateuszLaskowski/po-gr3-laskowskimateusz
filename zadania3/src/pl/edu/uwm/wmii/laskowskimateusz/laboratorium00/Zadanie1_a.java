package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1_a {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        if (n>0 && n<101){
            int[] tablica =  new int[n];
            int parzyste = 0;
            Random r = new Random();
            int range = 999 + 999 + 1;
            for (int i = 0; i<n; i++) {
                int int_random = r.nextInt(range)-999;
                tablica[i] = int_random;
                if(tablica[i] % 2 == 0){
                    parzyste++;
                }
                System.out.printf( "%d\n", tablica[i] );
            }
            System.out.printf("Parzyste = %d\n", parzyste);
            System.out.printf("Niearzyste = %d\n", n - parzyste);
        }
        else {
            System.out.println("Number out of range");
        }
    }
}