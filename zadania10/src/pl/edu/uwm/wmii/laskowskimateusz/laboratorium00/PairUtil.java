package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

public class PairUtil
{
    static <T> Pair<T> swap(Pair pair)
    {
        pair.swap();
        return pair;
    }
}