package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import pl.imiajd.laskowski.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String[] imiona = {"Mateusz", "Zyzz"};
        String nazwisko = "Laskowski";
        boolean plec = true;
        LocalDate dataUrodzenia = LocalDate.of(2000, 8, 12);
        Osoba osoba = new Osoba(imiona, nazwisko, plec, dataUrodzenia);

        System.out.println(osoba.getImiona()[0]);
        System.out.println(osoba.getNazwisko());
        System.out.println(osoba.getRokUrodzenia());
        System.out.println(osoba.getPlec() ? "mężczyzna" : "kobieta");

        Pracownik pracownik = new Pracownik(
                imiona,
                nazwisko,
                plec,
                dataUrodzenia,
                15000,
                LocalDate.of(2018, 1, 10));
        System.out.println(pracownik.getPensja());
        System.out.println(pracownik.getDataZatrudnienia());

        Student student = new Student(imiona, nazwisko, plec, dataUrodzenia, "Informatyka", 3.01);
        System.out.println(student.getKierunek());
        System.out.println(student.getSredniaOcen());
        student.setSredniaOcen(5);
        System.out.println(student.getSredniaOcen());
        System.out.println("");
        ArrayList<Instrument> orkiestra = new ArrayList<>(Arrays.asList(
                new Flet("Tony Dripano", LocalDate.ofYearDay(2000, 365)),
                new Fortepian("Czopin", LocalDate.ofYearDay(2020, 365)),
                new Skrzypce("Volvvs", LocalDate.ofYearDay(2010, 365)),
                new Skrzypce("Zyzz", LocalDate.ofYearDay(2010, 365)),
                new Flet("Ejczu Ebezoga", LocalDate.ofYearDay(2000, 365))));
        for (Instrument instrument : orkiestra) {
            instrument.dzwiek();
        }
        System.out.println(orkiestra);
    }
}
