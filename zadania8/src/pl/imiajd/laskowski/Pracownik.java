package pl.imiajd.laskowski;

import java.time.LocalDate;

public class Pracownik extends Osoba
{
    private int pensja;
    private LocalDate dataZatrudnienia;

    public Pracownik(String[] imiona, String nazwisko, boolean plec, LocalDate dataUrodzenia, int pensja, LocalDate dataZatrudnienia)
    {
        super(imiona, nazwisko, plec, dataUrodzenia);
        this.pensja = pensja;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public int getPensja() {
        return pensja;
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    @Override
    public String toString()
    {
        return super.toString() + this.pensja + "\n";
    }
}