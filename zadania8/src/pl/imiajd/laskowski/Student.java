package pl.imiajd.laskowski;

import java.time.LocalDate;

public class Student extends Osoba
{
    private String kierunek;
    private double sredniaOcen;

    public Student(String[] imiona, String nazwisko, boolean plec, LocalDate dataUrodzenia, String kierunek, double sredniaOcen)
    {
        super(imiona, nazwisko, plec, dataUrodzenia);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getKierunek()
    {
        return this.kierunek;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public String toString()
    {
        return super.toString() + this.kierunek + "\n";
    }
}