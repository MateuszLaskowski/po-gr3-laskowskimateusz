package pl.imiajd.laskowski;

import java.time.LocalDate;

public class Osoba
{
    private String nazwisko;
    private LocalDate dataUrodzenia;
    private String[] imiona;
    boolean plec;

    public Osoba(String[] imiona, String nazwisko, boolean plec, LocalDate dataUrodzenia)
    {
        this.imiona = imiona;
        this.nazwisko = nazwisko;
        this.plec = plec;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String[] getImiona()
    {
        return this.imiona;
    }

    public LocalDate getRokUrodzenia()
    {
        return this.dataUrodzenia;
    }

    public String getNazwisko()
    {
        return this.nazwisko;
    }

    public boolean getPlec()
    {
        return this.plec;
    }

    @Override
    public String toString()
    {
        return this.nazwisko + "\n" + this.dataUrodzenia + "\n";
    }
}