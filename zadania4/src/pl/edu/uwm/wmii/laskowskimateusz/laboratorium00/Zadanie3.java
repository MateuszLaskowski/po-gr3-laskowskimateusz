package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args){
        try {
            Scanner odczyt = new Scanner(new File(args[0]));
            String wyraz = args[1];
            int count = 0;
            while(odczyt.hasNext()){
                String Linia = odczyt.nextLine();
                for (int i = 0; i < Linia.length()-wyraz.length(); i++) {
                    int subStrcount = 0;
                    for (int j = 0; j < wyraz.length(); j++) {
                        if (wyraz.charAt(j) == Linia.charAt(i + j)) {
                            subStrcount++;
                        }
                    }
                    if (subStrcount == wyraz.length()) {
                        count++;
                    }
                }
            }
            System.out.println(count);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
