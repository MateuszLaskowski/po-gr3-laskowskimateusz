package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.File;


public class Zadanie2 {
    public static void main(String[] args){
        try {
            Scanner odczyt = new Scanner(new File(args[0]));
            char c = args[1].charAt(0);
            int count = 0;
            while(odczyt.hasNext()){
                String Linia = odczyt.nextLine();
                for (int i = 0; i<Linia.length(); i++){
                    if (Linia.charAt(i) == c){
                        count++;
                    }
                }
            }
            System.out.println(count);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
