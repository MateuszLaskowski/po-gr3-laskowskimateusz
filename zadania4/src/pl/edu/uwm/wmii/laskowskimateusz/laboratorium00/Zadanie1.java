package pl.edu.uwm.wmii.laskowskimateusz.laboratorium00;

import java.util.Arrays;
import java.util.Scanner;

public class Zadanie1 {

    public static void main(String[] args) {
        String string1 = "tetetetetetetete";
        String string2 = "89723654342897897789234";
        char char1 = 't';
        String substring = "tes";
        String repeat = "ho";
        int repeatCount = 3;
        char sep = '.';
        int step = 4;
        System.out.printf("countChar = %d\n", countChar(string1, char1));


        System.out.printf("countSubStr = %d\n", countSubStr(string1, substring));


        System.out.printf("middle = %s\n", middle(string1));


        System.out.printf("repeat = %s\n", repeat(repeat, repeatCount));


        System.out.print("where = ");
        int[] where = where(string1, substring);
        for (int i : where)
        {
            System.out.print(i + " ");
        }


        System.out.printf("\nchange = %s\n", change(string1));


        System.out.printf("nice = %s\n", nice(string2));


        System.out.printf("nice = %s\n", nice(string2, sep, step));
    }

    public static int countChar(String str, char c) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }

    public static int countSubStr(String str, String subStr) {
        int count = 0;
        for (int i = 0; i < str.length()-subStr.length(); i++) {
            int subStrcount = 0;
            for (int j = 0; j < subStr.length(); j++) {
                if (subStr.charAt(j) == str.charAt(i + j)) {
                    subStrcount++;
                }
            }
            if (subStrcount == subStr.length()) {
                count++;
            }
        }
        return count;
    }

    public static String middle(String str){
        return str.length() % 2 == 0 ? String.valueOf(str.charAt((str.length()/2) - 1))
                + str.charAt((str.length()/2)) : String.valueOf(str.charAt(str.length()/2));
    }

    public static String repeat(String str, int n){
        return String.valueOf(str).repeat(Math.max(0, n));
    }

    public static int[] where(String str, String subStr){
        int[] where = new int[str.length()];
        for (int i = 0; i <= str.length()-subStr.length(); i++) {
            int subStrcount = 0;
            for (int j = 0; j < subStr.length(); j++) {
                if (subStr.charAt(j) == str.charAt(i + j)) {
                    subStrcount++;
                }
            }
            if (subStrcount == subStr.length()) {
                where[i] = i;
            }
        }
        return Arrays.stream(Arrays.stream(where).sorted().toArray()).distinct().toArray();
    }

    public static String change(String str){
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(str);
        for (int i = 0; i<strBuff.length(); i++){
            strBuff.setCharAt(i, Character.isUpperCase(strBuff.charAt(i))
                    ? Character.toLowerCase(strBuff.charAt(i)) : Character.toUpperCase(strBuff.charAt(i)));
        }
        return strBuff.toString();
    }

    public static String nice(String str){
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(str);
        int count = 0;
        for (int i = strBuff.length(); i>0; i--){
            count++;
            if(count == 3 && i>1){
                strBuff.insert(i-1,',');
                count = 0;
            }
        }
        return strBuff.toString();
    }

    public static String nice(String str, char sep, int step){
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(str);
        int count = 0;
        for (int i = strBuff.length(); i>0; i--){
            count++;
            if(count == step && i>1){
                strBuff.insert(i-1, sep);
                count = 0;
            }
        }
        return strBuff.toString();
    }
}
